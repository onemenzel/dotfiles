# If you come from bash you might have to change your $PATH.
DOTFILES="$(dirname $(readlink -e $HOME/.zshrc))"

PATH_LOCAL="$HOME/bin:$HOME/.local/bin"
[[ -n "$XDG_DATA_HOME" ]] && PATH_LOCAL="$XDG_DATA_HOME/bin"
PATH_SYSTEM="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"
PATH_DOTFILES="$DOTFILES/bin"

# build PATH and remove double entries
export PATH=$(
  echo -n "$PATH_LOCAL:$PATH_DOTFILES:$PATH_SYSTEM:$PATH" \
  | awk -v RS=: '!($0 in a) {a[$0]; printf("%s%s", length(a) > 1 ? ":" : "", $0)}'
)

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="agnoster-mod"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# remove the double virtualenv entry in the prompt
export VIRTUAL_ENV_DISABLE_PROMPT="yes"

# load iTerm2 integration
test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

function iterm2_print_user_vars {
  iterm2_set_user_var gitBranch $( (git branch 2> /dev/null) | grep \* | cut -c3-)
}

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git wd docker docker-compose tmux
)

# per-machine zshrc addtitions
test -e "${HOME}/.zshrc.env" \
  && source "${HOME}/.zshrc.env" \
  || true

source $ZSH/oh-my-zsh.sh

# ========= USER CONFIG ========= #
FD_CMD=$((( $+commands[fd] )) && echo fd || echo fdfind)
BAT_CMD=$((( $+commands[bat] )) && echo bat || echo batcat)

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
  export VISUAL='vim'
elif [[ -n $VSC_TERMINAL ]]; then
  export EDITOR='code --wait'
  export VISUAL=$EDITOR
else
  export EDITOR='vim'
  export VISUAL='vim'
fi
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi
export VIEWER="$BAT_CMD"

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# ============ ZSH ============ #
autoload zmv

zle -N _put_date

autoload -U select-word-style
select-word-style bash # make kill-word stop at /

bindkey "^[^[[C" forward-word
bindkey "^[^[[D" backward-word
bindkey "^[[1;2D" backward-word
bindkey "^[[1;2C" forward-word
bindkey "^[Q" backward-kill-word
bindkey -r "^A"
bindkey "^A"     _put_date

function _put_date {
  LBUFFER+=$(date "+%Y-%m-%d")
  zle end-of-line
}

function _probe_path() {
	local pth
	for p in $@; do
		pth=$(realpath -q "$p" || echo)
	done
	echo $pth
}

function _fancy_dirname() {
  local dir=$([ -n "$2" ] && echo "$2" || cat)
  echo $dir | awk "NF{NF-=$1}1" FS='/' OFS='/'
}

# enable venvs automatically
function _virtualenv_auto_activate() {
  local a="bin/activate"
  local venv_path="$(_probe_path ENV/$a venv/$a .venv/$a | _fancy_dirname 2)"

  # Check to see if already activated to avoid redundant activating
  if [[ -n "$venv_path" && "$VIRTUAL_ENV" != "$venv_path" ]]; then
    echo "auto activate venv ${venv_path}"
    source "$venv_path/$a"
  elif [[ -n "$VIRTUAL_ENV" && "${PWD##$(dirname $VIRTUAL_ENV)}" == "${PWD}" ]]; then
    echo "deactivate venv"
    deactivate
    [ -f "$HOME/.pythonenv/bin/activate" ] && source "$HOME/.pythonenv/bin/activate"
  fi
}


# auto activate local venv
# enable by putting "DOT_AUTO_VENV=true" into .zshrc.env
if [[ $DOT_AUTO_VENV ]]; then
  chpwd_functions+=(_virtualenv_auto_activate)
  precmd_functions=(_virtualenv_auto_activate $precmd_functions)
fi

function _condaenv_auto_activate() {
  if [[ -f ".python-version" ]]; then
    local VENV=$(cat .python-version)

    # Check to see if already activated to avoid redundant activating
    if [[ "$CONDA_DEFAULT_ENV" != "$VENV" ]]; then
      echo "auto activate conda env ${VENV}"
      mamba activate $VENV
    fi
  fi
}

# auto activate condaenv if mamba is present.
# enable by putting "DOT_AUTO_CONDAENV=true" into .zshrc.env
if [[ $DOT_AUTO_CONDAENV && $(command -v mamba) ]]; then
  chpwd_functions+=(_condaenv_auto_activate)
  precmd_functions=(_condaenv_auto_activate $precmd_functions)
fi

# ====== CUSTOM FUNCTIONS ====== #
function things_today {
  if [[ $(command -v things >/dev/null) ]]; then
    # Show the content of "things" once per day
    THINGS_TOUCH="${HOME}/.things_date"
    [[ ! -f "$THINGS_TOUCH" ]] && touch "$THINGS_TOUCH"
    DATE_TODAY=$(date "+%Y-%m-%d")
    DATE_LAST_THINGS=$(date "+%Y-%m-%d" -r "$THINGS_TOUCH")
    if [ "$DATE_TODAY" != "$DATE_LAST_THINGS" ]; then
      things
      touch "$THINGS_TOUCH"
    fi
    unset DATE_TODAY
    unset THINGS_TOUCH
    unset DATE_LAST_THINGS
  fi
}

function rgf {
  rg $@ --column --no-heading --line-number --color=always \
  | fzf --preview="_fzf_preview.sh {}" --ansi
}

# ======== SYSTEMD Fix ======== #

_systemctl_unit_state() {
  typeset -gA _sys_unit_state
  _sys_unit_state=( $(__systemctl list-unit-files "$PREFIX*" | awk '{print $1, $2}') )
}

# ============ FZF ============ #
# Use fd (https://github.com/sharkdp/fd) instead of the default find
# command for listing path candidates.
# - The first argument to the function ($1) is the base path to start traversal
# - See the source code (completion.{bash,zsh}) for the details.
function _fzf_compgen_path {
  $FD_CMD --hidden --follow --exclude ".git/*" --exclude "*.swp" \
     --exclude "__pycache__" --exclude ".DS_Store" . "$1"
}

# Use fd to generate the list for directory completion
function _fzf_compgen_dir {
  $FD_CMD --type d --hidden --follow --exclude ".git/*"  --exclude "*.swp" \
     --exclude "__pycache__" --exclude ".DS_Store" . "$1"
}

export FZF_DEFAULT_COMMAND="$FD_CMD --type f --follow --hidden -E '.git/*' -E '*.swp' -E '__pycache__' -E '.DS_Store'"
# export FZF_COMPLETION_OPTS='+c -x' # Options to fzf command
# export FZF_COMPLETION_TRIGGER='~~' # Use ~~ as the trigger sequence instead of the default **
export FZF_PREVIEW_COMMAND="$BAT_CMD --color=always {}"

FZF_COMPPATH="/usr/share/doc/fzf/examples/completion.zsh"
test -f "$FZF_COMPPATH" && source "$FZF_COMPPATH" || true

# ============ BAT ============ #
export BAT_THEME="base16"
export BAT_PAGER="less -R"

# =========== LESS ============ #
export LESSCHARSET=utf-8

# ========== RIPGREP ========== #
export RIPGREP_CONFIG_PATH="$DOTFILES/ripgreprc"

# ============ SSH ============ #

# ssh agent autostart, e.g., to access private git repos
SSH_ENV="$HOME/.ssh/environment"

function start_agent {
     # Initialise new SSH agent.
     /usr/bin/ssh-agent | sed 's/^echo/# echo/' > "${SSH_ENV}"
     # echo succeeded
     chmod 600 "${SSH_ENV}"
     . "${SSH_ENV}" > /dev/null
     /usr/bin/ssh-add;
}

# Source SSH settings, if applicable
if [ -f "${SSH_ENV}" ]; then
    . "${SSH_ENV}" > /dev/null
    ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
        start_agent;
    }
elif [ "${ENABLE_SSH_AGENT:-false}" = "true" ]; then
    start_agent;
fi

# ========== ALIASES ========== #
# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
alias python="python3"
alias c="clear"
alias pip="pip3"
alias venv="source ENV/bin/activate"
alias bat="$BAT_CMD"
alias fd="$FD_CMD"
# alias rg0="$(which rg)"
# alias rg="rg ${RG_DEFAULT_OPTIONS}"
alias pfz="fzf --preview='${FZF_PREVIEW_COMMAND}'"
test $(command -v nvim) && alias vim="nvim"

# ======= EXECUTE CMDs ======= #
# things_today
# echo "TODO: add things to dotfiles"

# per-machine zshrc addtitions
test -e "${HOME}/.zshrc.custom" && source "${HOME}/.zshrc.custom" || true
