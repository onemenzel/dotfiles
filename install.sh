#!/bin/bash
set -eo pipefail

BLACK='\033[0;30m'
DARK_GRAY='\033[1;30m'
RED='\033[0;31m'
LIGHT_RED='\033[1;31m'
GREEN='\033[0;32m'
LIGHT_GREEN='\033[1;32m'
BROWN_ORANGE='\033[0;33m'
YELLOW='\033[1;33m'
BLUE='\033[0;34m'
LIGHT_BLUE='\033[1;34m'
PURPLE='\033[0;35m'
LIGHT_PURPLE='\033[1;35m'
CYAN='\033[0;36m'
LIGHT_CYAN='\033[1;36m'
LIGHT_GRAY='\033[0;37m'
WHITE='\033[1;37m'
COLOR_END='\033[0m'

function colored {
  printf "${1:-$WHITE}${2}${COLOR_END}"
}

function mv_msg {
  FROM="$1"
  if [ -e "$FROM" ]; then
    BAK="${2:-$FROM.bak}"
    colored $LIGHT_CYAN "found ${FROM}. moving to ${BAK}\n"
    mv "$1" "$BAK"
  else
    colored $LIGHT_CYAN "${FROM} does not exist. skipping backup.\n"
  fi
}

function read_while {
  QUESTION="$1"
  while true; do
    read -p "$QUESTION [y/n] " READ_WHILE_OUT

    if [ "$READ_WHILE_OUT" = y ]; then
      echo true
      break
    elif [ "$READ_WHILE_OUT" = n ]; then
      echo false
      break
    else
      colored $LIGHT_RED "Only 'y' or 'n' can be accepted here.\n" >&2
    fi
  done
}

function build_install_list {
  # not really needed but still keeping it
  INSTALL_LIST=""
  for PKG in "${1[@]}"; do
    [[ ! $(command -v "$PKG" >/dev/null) ]] && INSTALL_LIST="${INSTALL_LIST} $PKG"
  done
  echo $INSTALL_LIST
}

CAN_SUDO_OR_ROOT=false
if [[ $(
  groups $(whoami) | cut -d ':' -f 2 \
  | awk '{for (i=1; i<=NF; i+=1) {print $i}}' \
  | grep -E '^(root|admin|sudo)$'
) ]]; then
  CAN_SUDO_OR_ROOT=true
fi

function superexec {
  ERR_MSG="$1"
  shift

  if [ $UID -eq 0 ]; then
    $@
  elif [ $CAN_SUDO_OR_ROOT = true ]; then
    sudo $@
  else
    colored $LIGHT_RED "$ERR_MSG\n"
  fi
}

BASEDIR=$(cd `dirname "$0"` && pwd)
XDG_DATA_HOME=${XDG_DATA_HOME:-$HOME/.local}

set +x # show commands

# put xterm with italic font to term database
tic $BASEDIR/xterm-italic.tic

# assume debian based
REQ_PKGS="zsh git vim wget tmux gawk"
superexec "package lists could not be updated." apt update
superexec "required packages (${REQ_PKGS}) cannot be installed." apt install -y $REQ_PKGS

colored $LIGHT_CYAN "downloading submodules...\n"
cd "$BASEDIR"
git submodule init
git submodule update
colored $LIGHT_CYAN "done.\n"

cd $HOME

if [ ! -e ".oh-my-zsh" ]; then
  export RUNZSH="no"
  export CHSH="no"
  # sh wget I know...
  sh -c "$(wget https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
fi

mv_msg ".zshrc"
mv_msg ".tmux.conf"
mv_msg ".vim"
mv_msg ".vimrc"
mv_msg "$XDG_DATA_HOME/bin"
# mv_msg ".oh-my-zsh/custom" ".oh-my-zsh_custom.bak"
# if [[ -e ".oh-my-zsh" && ! -e ".oh-my-zsh/custom" ]]; then
#   colored $LIGHT_CYAN "recreating oh-my-zsh custom folder\n"
#   mkdir -p ".oh-my-zsh/custom"
# fi

colored $LIGHT_CYAN "linking dotfiles... "
# ln -s "$BASEDIR/oh-my-zsh/custom"/* "$HOME/.oh-my-zsh/custom/"
ln -s "$BASEDIR/zshrc" ".zshrc"
ln -s "$BASEDIR/tmux.conf" ".tmux.conf"
ln -s "$BASEDIR/vim" ".vim"
# copy archive mode (recursive, no dereference), make symbolic links, no overwrite
cp -asn "$BASEDIR/oh-my-zsh/custom"/* "$HOME/.oh-my-zsh/custom/"
# the following has been replaced by autoloading
# the dotfiles/bin directory PATH in zshrc
# ln -s "$BASEDIR/bin" "$XDG_DATA_HOME/bin"
colored $LIGHT_GREEN "done.\n"

# colored $LIGHT_CYAN "committing oh-my-zsh so it can be updated\n"
# cd ${HOME}/.oh-my-zsh
# git add .
# git -c user.name=${USER:-root} -c user.email=${USER:-root}@`hostname` commit -m "installed from dotfiles"
# cd ${HOME}

touch ${HOME}/.zshrc.env
touch ${HOME}/.zshrc.custom

ITERM_ENABLED=$(read_while "link iTerm2 compatibility script?")
if [ $ITERM_ENABLED = true ]; then
  mv_msg ".iterm2_shell_integration.zsh"
  ln -s "$BASEDIR/iterm2_shell_integration.zsh" ".iterm2_shell_integration.zsh"
fi

OPT_PKGS="fzf fd-find ripgrep bat progress"
if [ $CAN_SUDO_OR_ROOT = true ]; then
  INSTALL_OPTIONAL=$(read_while "install optional packages [${OPT_PKGS}]?")
  # error message is not needed here.
  if [ $INSTALL_OPTIONAL = true ]; then
    superexec "" apt install -y $OPT_PKGS
    # workaround: bat and rg cannot be installed together in ubuntu 20.04
    # cd /tmp
    # apt download bat
    # superexec "" dpkg --force-overwrite -i bat*.deb
    # cd $HOME
  fi
fi

colored $LIGHT_CYAN "setting up vim...\n"
vim +InstallPackages
colored $LIGHT_GREEN "done.\n"

colored $LIGHT_CYAN "changing the shell to zsh\n"
if [ $CAN_SUDO_OR_ROOT = true ]; then
  superexec "shell could not be changed." chsh -s /bin/zsh $USER
else
  chsh -s /bin/zsh
fi

colored $LIGHT_CYAN 'finished. you can now start using your environment!\n'
